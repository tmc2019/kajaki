import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GooglemapsComponent } from './googlemaps/googlemaps.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavmenuComponent } from './navmenu/navmenu.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    GooglemapsComponent,
    HomeComponent,
    LoginComponent,
    NavmenuComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyAMQFQHJnRqjP5Y_geHcRk6iiLuYDOCmD0'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
