package pl.tmc.kajaki.models.requests;

public class ChangePasswordRequest {

    public String username;
    public String password;
}
