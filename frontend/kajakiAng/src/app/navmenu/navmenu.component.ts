import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css']
})
export class NavmenuComponent implements OnInit {

  isLoggedIn = localStorage.getItem('login') !== null;
  firmLogin = '';

  constructor() { }

  ngOnInit() {
    if (this.isLoggedIn) {
      this.firmLogin = localStorage.getItem('login');
    }
  }

  onClickMe() {
    localStorage.clear();
    window.open('/home', '_self');
  }

}
