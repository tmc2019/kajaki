import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  public login: string;
  public password: string;
  public repeatPassword: string;
  public email: string;
  public phone: string;
  public name: string;
  public description: string;

  submitted = false;

  onSubmit(login: string, password: string, repeatPassword: string, email: string, phone: string, name: string, description: string) {
    this.submitted = true;
    setTimeout(() => {
      window.open('/login', '_self');
    },1000);
  }

}
