package pl.tmc.kajaki.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.tmc.kajaki.models.database.User;

public interface UserRepository extends JpaRepository<User, String> {

}
