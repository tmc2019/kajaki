import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  submitted = false;

  onSubmit(login: string, password: string) {
    this.submitted = true;
    localStorage.setItem('login', login);
    setTimeout(() => {
      window.open('/google-map', '_self');
    },1000);
  }

}
