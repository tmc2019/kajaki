import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { isListLikeIterable } from '@angular/core/src/change_detection/change_detection_util';

@Component({
  selector: 'app-googlemaps',
  templateUrl: './googlemaps.component.html',
  styleUrls: ['./googlemaps.component.css']
})
export class GooglemapsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  // google maps zoom level
  zoom: number = 10;

  // initial center position for the map
  lat: number = 54.0287383;
  lng: number = 19.03939639;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }


  mapClicked($event: MouseEvent, description: string) {
    if (localStorage.getItem('login') !== null) {
      this.markers.push({
        lat: $event.coords.lat,
        lng: $event.coords.lng,
        label: localStorage.getItem('login') || undefined,
        draggable: true,
        title: description
      });
    }
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
    m.lat = $event.coords.lat,
      m.lng = $event.coords.lng
  }

  markers: marker[] = [
    {
      lat: 51.673858,
      lng: 7.815982,
      label: 'A',
      draggable: true
    },
    {
      lat: 51.373858,
      lng: 7.215982,
      label: 'B',
      draggable: false
    },
    {
      lat: 51.723858,
      lng: 7.895982,
      label: 'C',
      draggable: true
    }
  ]
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  title?: string;
}
