package pl.tmc.kajaki.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.tmc.kajaki.models.database.User;
import pl.tmc.kajaki.models.database.UserRole;
import pl.tmc.kajaki.models.requests.ChangePasswordRequest;
import pl.tmc.kajaki.models.requests.RegisterRequest;
import pl.tmc.kajaki.models.requests.UpdateUserRquest;
import pl.tmc.kajaki.repositories.UserRepository;

import java.security.Principal;
import java.util.*;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

    @RequestMapping("/resource")
    public Map<String,Object> home() {
        Map<String,Object> model = new HashMap<String,Object>();
        model.put("id", UUID.randomUUID().toString());
        model.put("content", "Hello World");
        return model;
    }

    @GetMapping("/users")
    public List<User> users(){
        return userRepository.findAll();
    }

    @GetMapping("/users/{username}")
    public User user(@PathVariable("username") final String username){
        Optional<User> userOptional = userRepository.findById(username);

        User user = userOptional.orElse(null);
        return user;
    }

    @PostMapping("/change-password")
    public User changePassword(@RequestBody ChangePasswordRequest request){
        Optional<User> optionalUser = userRepository.findById(request.username);
        if(optionalUser.isPresent())
        {
            User user = optionalUser.get();
            user.setPassword(request.password);
            userRepository.save(user);
            return user;
        }
        return null;
    }

    @PostMapping("/register")
    public User register(@RequestBody RegisterRequest request) {
        User user = new User();
        user.setUsername(request.username);
        user.setPassword(request.password);
        user.setEnabled(true);
        user.setUserRoles(new HashSet<UserRole>());
        userRepository.save(user);
        return user;
    }

    @PostMapping("/users/update")
    public User update(@RequestBody UpdateUserRquest request) {
        Optional<User> optionalUser = userRepository.findById(request.username);
        if(optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setName(request.name);
            user.setLastName(request.lastName);
            userRepository.save(user);
            return user;
        }
        return null;
    }
}
